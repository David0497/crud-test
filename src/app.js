const express = require('express');
const app = express();

// config
app.set('port', process.env.PORT || 1234);

//extensions
const morgan = require('morgan');
const db = require('./database/conection')

// middlewares
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(morgan('dev'));

//router
const customersRouter = require('./router/customer')

app.use('/customers', customersRouter);

app.listen(app.get('port'), () => {
  console.log(`Server on port ${app.get('port')}`)
})




 
