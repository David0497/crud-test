const express = require('express');
const router = express.Router();
const db = require('../database/conection');
const employes = require('../controllers/employes');
const departments = require('../controllers/departments');

// departments
router.get('/departments', departments.getDepartments)

/** @todo: replace post by get and recover the data from the url params. **/
router.post('/department/get', departments.getDepartment) 

router.post('/department', departments.createDepartment)

router.put('/department', departments.updateDepartment)

router.delete('/department', departments.deleteDepartment )

// employes
router.get('/employes', employes.getEmployes)

/** @todo: replace post by get and recover the data from the url params. **/
router.post('/employee/get', employes.getEmploye)

router.post('/employee', employes.createEmploye )

router.put('/employee', employes.updateEmploye )

router.delete('/employee', employes.deleteEmploye )



module.exports = router;
