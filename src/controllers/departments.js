const db = require("../database/conection");

async function getDepartments (req, res) {
    try {
        var employes = await db.any('SELECT * FROM departamento');
        return res.status(201).json(employes);
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function getDepartment (req, res) {
    try {
        var department = await db.query(`SELECT * FROM departamento WHERE "codigo" = $1`,
             [req.body.codigo]);
        return res.status(201).json(department);
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function createDepartment (req, res) {
    try {
        console.log(req.body)
        await db.query(
            `INSERT INTO "departamento" ("nombre", "presupuesto")  
             VALUES ($1, $2)`, [req.body.nombre, req.body.presupuesto]);
        return res.status(201).json({message: 'Departamento creado exitosamente'});
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function updateDepartment (req, res) {
    try {
        var department = await db.query(
            `UPDATE "departamento" SET "nombre" = $1, "presupuesto" = $2 WHERE "codigo" = $3`,
             [req.body.nombre, req.body.presupuesto, req.body.codigo]);
        return res.status(201).json({message: 'Actualizado exitosamente'});
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function deleteDepartment (req, res) {
    try {
        await db.query(`DELETE FROM "departamento" WHERE "codigo" = $1`,
             [req.body.codigo]);
        return res.status(201).json({message: 'Eliminado exitosamente'});
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};


module.exports = {
    getDepartments,
    createDepartment,
    updateDepartment,
    deleteDepartment,
    getDepartment
}