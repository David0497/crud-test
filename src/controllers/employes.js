const db = require("../database/conection");

async function getEmployes (req, res) {
    try {
        var employes = await db.query('SELECT * FROM empleado');
        return res.status(201).json(employes);
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function getEmploye (req, res) {
    try {
        var employe = await db.any(`SELECT * FROM empleado WHERE "codigo" = $1`,
        [req.body.codigo]);
        return res.status(201).json(employe);
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function createEmploye (req, res) {
    try {
        console.log(req.body)
        await db.query(
            `INSERT INTO "empleado" ("nif", "nombre", "apellido1", "apellido2", "codigo_departamento")  
             VALUES ($1, $2, $3, $4, $5)`, [req.body.nif, req.body.nombre, req.body.apellido1, req.body.apellido2, req.body.codigo_departamento]);
        return res.status(201).json({message: 'Empleado creado exitosamente'});
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function updateEmploye (req, res) {
    try {
        var employe = await db.query(
            `UPDATE "empleado" SET "nif" = $1, "nombre" = $2, "apellido1" = $3, "apellido2" = $4
             WHERE "codigo" = $5`,
             [req.body.nif, req.body.nombre, req.body.apellido1, req.body.apellido2, req.body.codigo]);
        return res.status(201).json({message: 'Actualizado exitosamente'});
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};

async function deleteEmploye (req, res) {
    try {
        await db.query(
            `DELETE FROM "empleado" WHERE "codigo" = $1`, [req.body.codigo]);
        return res.status(201).json({message: 'Eliminado exitosamente'});
    } catch (error) {
        console.error(error);
        return res.status(500).json({message: error.message ? error.message : 'Ha ocurrido un error'});
    } 
};



module.exports = {
    getEmployes,
    createEmploye,
    updateEmploye,
    deleteEmploye,
    getEmploye
}