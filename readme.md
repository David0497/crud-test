### STEP 1

```
docker-compose up -d
```

### STEP 2

Connection to the database using external manager database and use the next credentials.

username = user
password = password
database = testdb

```
CREATE TABLE departamento (
	codigo serial primary key, 
	nombre varchar(100) not null unique,
	presupuesto float
);

CREATE TABLE empleado (
	codigo serial primary key,
	nif varchar(9) not null unique,
	nombre varchar(100) not null,
	apellido1 varchar(100) not null,
	apellido2 varchar(100) not null,
	codigo_departamento integer not null,
	foreign key (codigo_departamento) references departamento(codigo)
);
```

### STEP 3

Now the microservice is ready